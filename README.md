# Busca de produtos em React utilizando Firebase Hosting, Firebase Functions e NextJs

## Tecnologias
* React com TypeScript
* Controle de estados com MobX
* NextJs para ServerSideRendering (SSR) e servidor de dev
* Firebase Functions para o backend
* Firebase RealTimeDatabase como banco de dados

## Instalação

### Set up firebase:
Para rodar o backend local, é necessário instalar o firebase, logar e criar um projeto.

* instalar Firebase Tools: `npm i -g firebase-tools`
* criar um projeto no firebase pelo [firebase web console](https://console.firebase.google.com/)
* coletar o ID do projeto: https://console.firebase.google.com/project/<projectId>
* atualizar `.firebaserc` project ID  para o criado
* login no Firebase CLI tool com `firebase login`

#### Instalar projeto:

```bash
npm install
```

#### Run Next.js development:

```bash
npm run dev
```

#### Run Firebase locally for testing:

```
npm run serve
```

## Hospedagem
DEMO: https://out-igqlensziz.now.sh/
Com a utilização do Next e o plano básico do Firebase, não é possível acessar através do dominio apresentado. Portanto, deve-se executar em modo de desenvolvimento.


## API
https://us-central1-products-search-a5364.cloudfunctions.net/api/search/

```
/api/search/{TITULO}?size={QTD_REGISTROS}&page={PAGINAÇÂO}
```
* TITULO: string com o termo de busca. Caso vazia mostra todos os produtos
* QTD_REGISTROS: Quantidade de registros que será mostrado
* PAGINAÇÂO: Posição da página. 
