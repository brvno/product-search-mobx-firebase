import { action, observable, runInAction } from 'mobx';
import {Product, SearchProductResponse} from './models/Product';
import productService from './services/ProductService';

let store: Store | null = null;

export class Store {
  @observable public lastUpdate;
  @observable public query!: string;
  @observable public productList!: Product[];
  @observable public total!: number;
  @observable public page!: number;
  @observable public size!: number;

  constructor(isServer, lastUpdate) {
    this.init(lastUpdate);
    this.searchProducts('');
  }

  @action public init = (lastUpdate) => {
    this.lastUpdate = lastUpdate || 0;
    this.query = '';
    this.productList = [];
    this.total = 0;
    this.page = 1;
    this.size = 4;
  }

  @action
  public async fetchProducts(newQuery: string) {
      this.productList = [];
      try {
          const response: SearchProductResponse = await productService.searchProduct(newQuery, this.page, this.size);
          // after await, modifying state again, needs an actions:
          runInAction(() => {
            this.productList = {...response.products};
            this.total = response.total;
          });
      } catch {
        //
      }
  }

  @action public searchProducts = (newQuery) => {
    this.query = newQuery;
    this.fetchProducts(newQuery);
  }

  @action public changeStep = (value: number) => {
    this.size = value;
    this.page = 1;
    this.searchProducts(this.query);
  }

  @action public changePage = (value: number) => {
    this.page = value;
    this.searchProducts(this.query);
  }
}

export function initializeStore(isServer, lastUpdate = Date.now()) {
  if (isServer) {
    return new Store(isServer, lastUpdate);
  } else {
    if (store === null) {
      store = new Store(isServer, lastUpdate);
    }
    return store;
  }
}
