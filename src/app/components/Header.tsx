import { inject, observer } from 'mobx-react';
import React from 'react';
import SearchBox from './SearchBox';
import StoreComponent from './StoreComponent';

const style = {
  container: {
    alignItems: 'center',
    display: 'flex',
    justifyContent: 'space-between',
    margin: '0 20px',
  },
  logo: {
    margin: '15px',
  },
  query: {
    alignItems: 'center',
    backgroundColor: '#edecf0',
    borderColor: 'rgb(185, 185, 185)',
    borderStyle: 'solid',
    borderWidth: '1px',
    borderLeft: 0,
    borderRight: 0,
    color: 'rgb(88,88,90)',
    display: 'flex',
    fontFamily: 'Lato, sans-serif',
    fontSize: '32px',
    fontWeight: '300',
    height: '80px',
    paddingLeft: '30px',
    marginBottom: '20px',
  },
};

@inject('store') @observer
class Header extends StoreComponent {
  public render() {

    const { store } = this.injected;
    return (
      <div >
        <div style={style.container}>
            <a><img style={style.logo} src='https://mmartan.com.br/img/logo.svg'/></a>
            <SearchBox />
        </div>
        <div style={style.query as any}>
          <span>{store.query ? store.query : 'Lista de Produtos'}</span>
        </div>
      </div>
    );
  }
}

export default Header;
