import { inject, observer } from 'mobx-react';
import * as React from 'react';
import StoreComponent from './StoreComponent';

const style = {
    input: {
        background: 'url("https://mmartan.com.br/img/icons/search-3.svg") no-repeat',
        backgroundPosition: '15px center',
        backgroundSize: '20px',
        borderColor: 'rgb(187,187,187)',
        borderRadius: '30px',
        borderStyle: 'solid',
        borderWidth: '1px',
        color: 'rgb(140,140,140)',
        padding: '5px 20px 5px 60px',
        width: '20%',
        outline: 'none',
        boxSizing: 'content-box',
    },
};

@inject('store') @observer
export default class SearchBox extends StoreComponent {
    public state = {
        query: this.injected.store.query,
    };

    public render() {
        return(
            <input
                style={style.input as any}
                type='search'
                value={this.state.query}
                onKeyUp={this.handleSubmit}
                onChange={this.handleChange}
            />
        );
    }

    private handleChange = (e) => {
        // this.props.store.changeQuery(e.target.value);
        this.setState({
            query: e.target.value,
        });
    }

    private handleSubmit = (e) => e.key === 'Enter' && this.injected.store.searchProducts(this.state.query);
}
