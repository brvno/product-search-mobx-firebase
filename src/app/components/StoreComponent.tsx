import * as React from 'react';
import {Store} from '../store';

interface InjectStore {
    store: Store;
}

export default class StoreComponent extends React.Component {
    get injected() {
        return this.props as InjectStore;
    }
}
