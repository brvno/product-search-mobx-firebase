import { Product } from '../models/Product';

const styles = {
    container: {
        borderColor: 'rgb(237, 234, 233)',
        borderStyle: 'solid',
        borderWidth: '1px',
        borderBottomWidth: '0px',
        display: 'flex',
        flexDirection: 'row',
        fontFamily: 'Lato, sans-serif',
        justifyContent: 'space-between',
        padding: '5px',
        paddingLeft: '21px',
    },
    description: {
        category: {
            color: 'rgb(88,88,90)',
            fontSize: '12px',
        },
        container: {
            marginLeft: '16px',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
        },
        title: {
            color: 'rgb(0,0,0)',
            fontSize: '16px',
        },
    },
    img: {
        maxWidth: '80px',
        maxHeight: '80px',
        marginRight: '5px',
    },
    price: {
        container: {
            margin: 'auto 0',
            paddingRight: '21px',
        },
        list: {
            color: 'rgb(109, 110, 112)',
            fontSize: '15px',
            textDecoration: 'line-through',
        },
        sale: {
            color: 'rgb(51,51,51)',
            fontSize: '15px',
        },
    },
};

const formatPrice = (price: number) => `R$ ${price / 100},${price % 100 < 10 ? '0' + price % 100 : price % 100}`;

const ProductItem = (props) => {
    const product: Product = props.product;
    const imageURl = `https://dgn7v532p0g5j.cloudfront.net/unsafe/120x120${product.photoStill}`;

    return (
        <div style={styles.container as any}>
            <div style={{display: 'flex'}}>
                <img style={styles.img} src={imageURl} />
                <img style={styles.img} src={imageURl} />
                <img style={styles.img} src={imageURl} />
                <img style={styles.img} src={imageURl} />
            <div style={styles.description.container as any}>
                <span style={styles.description.title}>{product.title}</span>
                <span style={styles.description.category}>{product.line} - {product.size}</span>
            </div>
            </div>
            <div style={styles.price.container}>
                <span style={styles.price.list}>{formatPrice(product.price.max)}</span>
                <span> por </span>
                <span style={styles.price.sale}>{formatPrice(product.price.min)}</span>
            </div>
        </div>
    );
};

export default ProductItem;
