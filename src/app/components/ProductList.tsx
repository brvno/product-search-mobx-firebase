import {toJS} from 'mobx';
import { inject, observer } from 'mobx-react';
import * as React from 'react';
import ProductItem from './ProductItem';
import StoreComponent from './StoreComponent';

const style = {
    container: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        margin: '0 auto',
        width: '80%',
    },
    result: {
        color: 'rgb(56, 56, 57)',
        paddingBottom: '5px',
        marginBottom: '31px',
        fontSize: '13px',
        fontWeight: '400',
        borderBottom: '3px solid rgb(223, 190, 127)',
    },
    footer: {
        container: {
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
            paddingTop: '20px',
            borderTop: '1px solid rgb(237, 234, 233)',
        },
        select: {
            height: '45px',
            padding: '0 20px',
            fontSize: '14px',
        },
        pages: {
            container: {
                display: 'flex',
            },
            items: {
                padding: '5px',
                width: '30px',
                margin: '10px',
                background: 'none',
                textAlign: 'center',
                cursor: 'pointer',
                border: 'none',
            },
            next: {
                background: 'url("./static/chevron-right.svg") no-repeat',
                backgroundPositionX: 'center',
                margin: '10px 0',
                paddin: '5px 0',
            },
            back: {
                background: 'url("./static/chevron-left.svg") no-repeat',
                margin: '10px 0',
                paddin: '5px 0',
                backgroundPositionX: 'center',
            },
            first: {
                background: 'url("./static/fast-backward.svg") no-repeat',
                margin: '10px 0',
                paddin: '5px 0',
                backgroundPositionX: 'center',
            },
            last: {
                background: 'url("./static/fast-forward.svg") no-repeat',
                margin: '10px 0',
                paddin: '5px 0',
                backgroundPositionX: 'center',
            },
            active: {
                border: '1px solid',
            },
        },
    },
};

@inject('store') @observer
export default class ProductList extends StoreComponent {
    public render() {
        const {store} = this.injected;

        return (
            <div style={style.container as any}>
                <div style={{display: 'flex', marginTop: '32px'}}>
                    <div style={style.result as any}>{store.total ? store.total + ' PRODUTOS ENCONTRADOS' : ''}</div>
                </div>
                <div >
                    {this.renderProducts()}
                </div>
                <div>
                    {this.renderFooter()}
                </div>
            </div>
        );
    }

    private renderProducts() {
        return Object.keys(toJS(this.injected.store.productList)).map((key) => {
            return (
                <ProductItem key={key} product={this.injected.store.productList[key]} />
            );
        });
    }

    private handleSelect = (e) => {
        this.injected.store.changeStep(parseInt(e.target.value, 10));
    }

    private handleClickPage = (e) => {
        let val = e.target.value.split('step-')[1];
        const { store } = this.injected;

        if (store.page === 1 && (val === 'init' || val === 'back')) {
            return;
        }

        if (store.page === store.total / store.size && (val === 'next' || val === 'last')) {
            return;
        }

        switch (val) {
            case 'init':
                val = 1; break;
            case 'next':
                val = store.page + 1; break;
            case 'back':
                val =  store.page - 1; break;
            case 'last':
                val = store.total / store.size; break;
            default: break;
        }
        store.changePage(parseInt(val, 10));
    }

    private renderFooter() {
        const select = (
            <select style={style.footer.select} onChange={this.handleSelect} value={this.injected.store.size}>
                <option value='4'>4 produtos por página</option>
                <option value='8'>8 produtos por página</option>
                <option value='16'>16 produtos por página</option>
                <option value='32'>32 produtos por página</option>
                <option value='64'>64 produtos por página</option>
            </select>
        );

        const pageMax = this.injected.store.total / this.injected.store.size;
        const pages: JSX.Element[] = [];
        const pageInit = (this.injected.store.page - 2 > 0 ? this.injected.store.page - 2 : 1);

        let iconStyle = {...style.footer.pages.items, ...style.footer.pages.first, };
        const goFirst: JSX.Element = <button style={iconStyle} value='step-init' onClick={this.handleClickPage}/>;

        iconStyle = {...style.footer.pages.items, ...style.footer.pages.last};
        const goLast: JSX.Element = <button style={iconStyle} value='step-last' onClick={this.handleClickPage}/>;

        iconStyle = {...style.footer.pages.items, ...style.footer.pages.back};
        const goBack: JSX.Element = <button style={iconStyle} value='step-back' onClick={this.handleClickPage}/>;
        iconStyle = {...style.footer.pages.items, ...style.footer.pages.next};
        const goNext: JSX.Element = <button style={iconStyle} value='step-next' onClick={this.handleClickPage}/>;
        pages.push(goFirst);
        pages.push(goBack);

        for (let i = pageInit; i < pageInit + 5 && i < pageMax + 1; i++) {
            const key = 'step-' + i;
            const isActive = this.injected.store.page === i ? style.footer.pages.active : {};
            const btnStyle = {...style.footer.pages.items, ...isActive};
            pages.push(
                <button
                    key={key}
                    value={key}
                    onClick={this.handleClickPage}
                    style={btnStyle as any}
                >
                    {i}
                </button>
            );
        }
        pages.push(goNext);
        pages.push(goLast);
        return (
            <div style={style.footer.container}>
                {select}
                <div  style={style.footer.pages.container}>
                    {pages}
                </div>
            </div>
        );

    }
}
