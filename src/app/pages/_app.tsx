import * as mobx from 'mobx';
import { Provider } from 'mobx-react';
import App, {Container} from 'next/app';
import Head from 'next/head';
import React from 'react';
import withMobxStore from '../lib/with-mobx-store';

mobx.configure({enforceActions: 'always'});

class MyApp extends App<any> {
  constructor(props) {
    super(props);
  }
  public render() {
    const {Component, pageProps, mobxStore} = this.props;
    return (
      <Container>
        <Head>
          <title>Lista Produtos - Mmartan</title>
          <meta charSet='utf-8' />
          <meta name='viewport' content='initial-scale=1.0, width=device-width' />
          <link rel='stylesheet' href='../static/reset.css' />
        </Head>
        <Provider store={mobxStore}>
          <Component {...pageProps} />
        </Provider>
      </Container>
    );
  }
}

export default withMobxStore(MyApp);
