import React from 'react';
import Header from '../components/Header';
import ProductList from '../components/ProductList';

export default class Index extends React.Component {
  public render() {
    return (
      <div>
        <Header />
        <ProductList />
      </div>
    );
  }
}
