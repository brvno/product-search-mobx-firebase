export interface Product {
    available: boolean;
    groupKey: string;
    line: string;
    photoStill: string;
    price: Price;
    size: string;
    sku: string;
    title: string;
    url: string;
}

export interface Price {
    min: number;
    max: number;
}

export interface SearchProductResponse {
    products: Product[];
    total: number;
}
