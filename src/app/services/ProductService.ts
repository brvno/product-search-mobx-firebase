
import fetch from 'node-fetch';
import {SearchProductResponse} from '../models/Product';

class ProductService {
    private readonly API_BASE_URL: string = 'https://us-central1-products-search-a5364.cloudfunctions.net/api';
    private readonly API_SEARCH_URL: string = `${this.API_BASE_URL}/search/`;

    public async searchProduct(query: string, page: number = 0, size: number = 4): Promise<SearchProductResponse> {
        const res: any = await fetch(`${this.API_SEARCH_URL}${query}?page=${page}&size=${size}`);
        const json: SearchProductResponse = await res.json();
        return json;
    }
}

const productService = new ProductService();

export default productService;
