import * as functions from 'firebase-functions';
import productService from '../services/product-service';
const cors = require('cors')({origin: true});

export const api = functions.https.onRequest(async (req, res) => {
    cors(req, res, ()=>{});
    const path = req.path.split('/')[1];

    switch(path.toLocaleLowerCase()){
        case 'populate':
            await resolvePopulatePath(req, res);
            break;
        case 'search':
            await resolveSearchPath(req,res);
            break;
        default:
            res.status(400).send({error: 'path not valid'});
    }
})

async function resolvePopulatePath  (req, res) {
    const paths = req.path.split('/');
    const name = paths[2];
    
    if(name){
        res.status(201).send({ products: await productService.populateProducts(name, req.query.size)})
        return;
    }        
    res.status(400).send({error: 'path not valid'});
    
}

async function resolveSearchPath (req, res) {
    try {
        const paths = req.path.split('/');
        const name = paths[2] || '';
        const query = req.query;
        
        const products = await productService.search(name, parseInt(req.query.page) || 1, parseInt(req.query.size) || 8);
        res.status(200).send(products)
    } catch(e) {
        res.status(500).send();
    }
}