import * as firebase from 'firebase-admin';
import fetch from 'node-fetch';

import Product from '../model/Product';

class ProductService {
    readonly API_BASE_URL: string = 'https://us-central1-products-search-a5364.cloudfunctions.net/';
    readonly API_SEARCH_MMARTAN: string = 'https://mmartan.com.br/api/search/query';
    
    public async getMmartanProducts(query: string, limit: number){ 
        const res = await fetch(this.API_SEARCH_MMARTAN, { 
            method: 'POST',
            body:    JSON.stringify({
                query,
                types: [{
                    limit,
                    offset: 0,
                    type: 'product'
                }]
            }),
            headers: { 'Content-Type': 'application/json' , 'Accept': 'application/json'},
        })
        return await res.json();
    }

    public async populateProducts(productName: string, limit: number = 20) {
        const result: any = (await this.getMmartanProducts(productName, limit));
 
        if(!result.data[0]) {
            return [];
        }
      
        const response = [];
        result.data[0].items.map(item => {
            const _product = item as Product;
            const uid = _product.sku.replace('.', '');
            response.push(uid);
            firebase.database().ref(`products/${uid}`).set(_product).catch();
        })
        return response;
    }

    public async search(query: string, page: number, size: number) {
        const snap = (await firebase.database().ref(`products`)
            .orderByKey()
            .once('value')).val();
        const products = [];

    for (const key in snap) {
            const item: Product = snap[key];
            item.title.toLocaleLowerCase()
                .includes(query.toLocaleLowerCase()) 
                ? products.push(item): null;
        }

        const initial: number = (page-1)*size
        const ending: number = initial + size;
        const result = []
        
        for(let i = initial; i < ending && i < products.length; i++) {
            result.push(products[i]);
        }
        
        return { total: products.length, products: result};
    }
}

const productService = new ProductService();

export default productService;