import * as functions from 'firebase-functions';
import * as firebase from 'firebase-admin';

const config = {
    apiKey: "AIzaSyDRap7AD8EUtalVK8bvgLddwaOIkqF1Ocs",
    authDomain: "products-search-a5364.firebaseapp.com",
    databaseURL: "https://products-search-a5364.firebaseio.com",
    storageBucket: "bucket.appspot.com"
};

firebase.initializeApp(config);

export { nextApp } from './app/next';
export { api } from './app/api';