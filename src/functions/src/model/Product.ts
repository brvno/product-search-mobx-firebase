export default interface Product {
    available: boolean;
    groupKey: string;
    line: string;
    photoStill: string;
    price: Price;
    size: string;
    sku: string;
    title: string;
    url: string;
}

